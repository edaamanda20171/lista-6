package br.ufc.eda.q1

class Conjunto {
  
  	var valores : Array[String] = null
  	var bits: Int = 0
  
  	def criar(n : Int, valores : Array[String]) {
    		this.valores = valores
    		bits = 0
  	}
  
  	def inserir(valor : String) {
  		for(i <- 0 to valores.length-1){
  			if(valores(i).equals(valor)){
  				ligarBit(i)
  				return
  			}
  		}
  	}
  
  	def remover(valor : String) {
  		for(i <- 0 to valores.length-1){
  			if(valores(i).equals(valor)){
  				desligarBit(i)
  				return
  			}
  		}
  	}
  
  	def uniao(conj : Conjunto) : Int = {
  		conj.bits | bits
  	}
  
  	def intersecao(conj : Conjunto) : Int = {
  		conj.bits & bits
  	}
  
    def diferenca(conj : Conjunto) : Int = {
  		if(conj.bits < bits)
  		  bits - conj.bits
  		else
  			conj.bits - bits
  	}
  
  	def verificarSubconjunto(conj : Conjunto) : Boolean = {
  		if((bits & conj.bits) == conj.bits && (bits | conj.bits) == bits)
  			true
  		false
  	}
  
  	def verificarConjuntosIguais(conj : Conjunto) : Boolean = {
  		(bits == conj.bits)
  	}
  
  	def complemento() : Int = {
  		~bits
  	}
  
  	def pertence(valor : String) : Boolean = {
  		for(i <- 0 to valores.length-1)
  			if(valores(i).equals(valor))
  				ligado(i)
  		
  		false
  	}
  
  	def numeroElementosConjunto() : Int = {
  		var valor : Int = bits
  		var bit : Int = 0
  		var contador : Int = 0
  		
  		while(valor != 0){
  			bit = valor%2
  			if(bit == 1)
  				contador += 1
  			valor = valor/2
  		}
  		
  		contador
  	}
  
  	def liberaConjunto() {
  		for(i <- 0 to valores.length-1){
  			valores(i) = null
  		}
  		valores = null
  	}
  	
  	def ligarBit(i : Int) {
  		if(!ligado(i))
  			bits += scala.math.pow(2, i).toInt
  	}
  	
  	def desligarBit(i : Int) {
  		if(ligado(i))
  			bits -= scala.math.pow(2, i).toInt
  	}
  	
  	def ligado(i : Int) : Boolean = {
  		var mask : Int =  1 << i
  		((mask & bits) != 0)
  	}
  	
  	def imprimirConjunto() {
  		for(i <- 0 to valores.length-1){
  			if(ligado(i))
  				println(valores(i))
  		}
  		println()
  	}
}