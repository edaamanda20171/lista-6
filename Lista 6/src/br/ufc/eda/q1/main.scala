package br.ufc.eda.q1

object main {
   def main(args: Array[String]): Unit = {
     
     var valores: Array[String] = new Array [String](3)
     valores = Array ("3", "0", "7")
     var tam: Int = 3
     var conj : Conjunto = new Conjunto()
     
     // item 1
     conj.criar(tam, valores)
		
     //item 2
		conj.inserir("3")
		conj.inserir("0")
		conj.inserir("7")
		conj.imprimirConjunto()
		
		// item 3
		conj.remover("2")
		conj.imprimirConjunto()
		
		var conj2 : Conjunto = new Conjunto()	
		conj2.criar(tam, valores)
		conj2.inserir("3")
		conj2.inserir("8")
		
		// item 4
		conj2.bits = (conj.uniao(conj2))
		
		conj2.imprimirConjunto()
		
		var conj3 : Conjunto = new Conjunto()	
		conj3.criar(tam, valores)
		conj3.inserir("2")
		conj3.inserir("1")
		
		// item 5
		conj3.bits = (conj.intersecao(conj3))
		conj3.imprimirConjunto()
		
		
		// item 6
		conj2.bits = (conj2.diferenca(conj3))
		
		println("CONJUNTO 2")
		conj2.imprimirConjunto()
		println("CONJUNTO 1")
		conj.imprimirConjunto()
		
		// item 7
		if(conj.verificarSubconjunto(conj2))
			println("Conjunto 2 é subconj de conj 1")
		else
			println("Conjunto 2 não é subconj de conj 1")
		
			// item 8
		if (conj.verificarConjuntosIguais(conj2))
			println("Iguais")
		else
			println("Diferentes")
		
			// item 9
		conj3.bits = (conj.complemento())
		conj3.imprimirConjunto()
		
		//item 10
		if(conj.pertence("1"))
			println("Pertence ")
		else
			println("Não Pertence ")
		
			
			//item 11
		println(conj.numeroElementosConjunto())
		
		
		//item 12
		conj.liberaConjunto()
		conj2.liberaConjunto()

    
  }
}